﻿using UnityEngine;

public class StompEnemy : MonoBehaviour
{
	public GameObject explosion;
	private Rigidbody2D playerRigidbody2D;
	// Use this for initialization
	void Start ()
	{
		playerRigidbody2D = transform.parent.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Enemy")
		{
			//Destroy(other.gameObject);
			other.gameObject.SetActive(false);
			playerRigidbody2D.velocity=new Vector3(playerRigidbody2D.velocity.x,10,0f);
			Instantiate(explosion, other.transform.position, other.transform.rotation);
		}
		if (other.tag == "Boss")
		{
			playerRigidbody2D.velocity = new Vector3(playerRigidbody2D.velocity.x, 10, 0f);
			other.transform.parent.GetComponent<BossScript>().takeDamage = true;

		}
	}
}
