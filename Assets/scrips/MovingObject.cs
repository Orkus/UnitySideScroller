using System.Collections;
using UnityEngine;

public class MovingObject : MonoBehaviour
{

    public GameObject objectToMove;

    public Transform startPoint;
    public Transform endPoint;

    public float moveSpeed;
    public float timeToWaitOnEnd;

    private Vector3 currentTarget;

	// Use this for initialization
	void Start ()
	{
	    currentTarget = endPoint.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, currentTarget,
	        moveSpeed * Time.deltaTime);
	    if (objectToMove.transform.position == endPoint.position)
	    {
            StartCoroutine("MakeWaitStartCo");
        }
	    if (objectToMove.transform.position == startPoint.position)
	    {
            StartCoroutine("MakeWaitEndCo");
	    }
     }

    public IEnumerator MakeWaitStartCo()
    {
        yield return new WaitForSeconds(timeToWaitOnEnd);
        currentTarget = startPoint.position;
    }

    public IEnumerator MakeWaitEndCo()
    {
        yield return new WaitForSeconds(timeToWaitOnEnd);
        currentTarget = endPoint.position;

    }

}
