using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour
{
	public string levelSelect;
	public string mainMenu;

	public LevelManager theLevelManager;
	public PlayerController thePlayerController;

	public GameObject thePauseScreen;

	// Use this for initialization
	void Start ()
	{
		theLevelManager = FindObjectOfType<LevelManager>();
		thePlayerController = FindObjectOfType<PlayerController>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (Time.timeScale == 0f)
			{
				ResumeGame();
			}
			else
			{
				PauseGame();
			}

		}

	}

	public void PauseGame()
	{
		Time.timeScale = 0;

		thePauseScreen.SetActive(true);
		thePlayerController.canMove = false;
		theLevelManager.gameMusic.Pause();

	}

	public void ResumeGame()
	{
		thePlayerController.canMove = true;
		theLevelManager.gameMusic.Play();
		Time.timeScale = 1f;
		thePauseScreen.SetActive(false);
	
	}

	public void LevelSelect()
	{
		Time.timeScale = 1f;
		PlayerPrefs.SetInt("PlayerLives",theLevelManager.currentLives);
		PlayerPrefs.SetInt("CoinCount",theLevelManager.coinCount);
		SceneManager.LoadScene(levelSelect);
	}

	public void QuitToMainMenu()
	{
		Time.timeScale = 1f;
		SceneManager.LoadScene(mainMenu);
	}
}
