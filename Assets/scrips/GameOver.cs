using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
	public string levelSelect;
	public string mainMenu;
	private LevelManager theLevelManager;

	// Use this for initialization
	void Start ()
	{
		theLevelManager = FindObjectOfType<LevelManager>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void RestartLevel()
	{
		PlayerPrefs.SetInt("CoinCount",0);
		PlayerPrefs.SetInt("PlayerLives",theLevelManager.startLives);
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void LevelSelection()
	{
		PlayerPrefs.SetInt("CoinCount", 0);
		PlayerPrefs.SetInt("PlayerLives", theLevelManager.startLives);
		SceneManager.LoadScene(levelSelect);
	}

	public void MainScreen()
	{
		SceneManager.LoadScene(mainMenu);
	}

}
