﻿using UnityEngine;

public class KillEnemy : MonoBehaviour
{
	public GameObject explosion;
	private PlayerController thePlayerController;
	public float bouncePlayer;
	// Use this for initialization
	void Start ()
	{
		thePlayerController = FindObjectOfType<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player" && thePlayerController.myRigidbody.velocity.y>0)
		{
			 Destroy(gameObject);
			Instantiate(explosion, transform.position, transform.rotation);
			thePlayerController.myRigidbody.velocity=new Vector3(thePlayerController.myRigidbody.velocity.x,bouncePlayer,0f);
		}
	}

}
