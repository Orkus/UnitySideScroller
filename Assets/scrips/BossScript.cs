using UnityEngine;

public class BossScript : MonoBehaviour
{
	public bool bossActive;

	public float timeBetweenDrops;
	private float timeBetweenDropsStore;
	public float dropCount;

	public float waitForPlatforms;
	private float platformCount;

	public Transform leftPoint;
	public Transform rightPoint;
	public Transform dropSawSpawnPoint;
	
	public GameObject dropSaw;
	public GameObject theBoss;

	public bool bossRight;

	public GameObject rightPlatform;
	public GameObject leftPlatform;

	public bool takeDamage;
	public int startingHealth;
	public int currentHealth;
	public GameObject levelExit;
	private CameraController theCameraController;
	public Transform cameraTarget;
	private LevelManager theLevelManager;
	public bool waitingForRespawn;
	// Use this for initialization
	void Start ()
	{
		timeBetweenDropsStore = timeBetweenDrops;
		theLevelManager = FindObjectOfType<LevelManager>();
		theCameraController = FindObjectOfType<CameraController>();
		dropCount = timeBetweenDrops;
		platformCount = waitForPlatforms;

		theBoss.transform.position = rightPoint.position;
		bossRight = true;
		currentHealth = startingHealth;

	}
	
	// Update is called once per frame
	void Update () {

		if (theLevelManager.respawnCoActive)
		{
			bossActive = false;
			waitingForRespawn = true; 
		}

		if (waitingForRespawn && !theLevelManager.respawnCoActive)
		{
			theBoss.SetActive(false);
			leftPlatform.SetActive(false);
			rightPlatform.SetActive(false);

			timeBetweenDrops = timeBetweenDropsStore;

			platformCount = waitForPlatforms;
			dropCount = timeBetweenDrops;

			theBoss.transform.position = rightPoint.position;
			bossRight = true;
			currentHealth = startingHealth;
			theCameraController.followTarget = true;
			waitingForRespawn = false;
		}

		if (bossActive)
		{	theBoss.SetActive(true);

			theCameraController.followTarget = false;
			theCameraController.transform.position = Vector3.Lerp(theCameraController.transform.position,
				cameraTarget.position, theCameraController.smoothing * Time.deltaTime);

			if (dropCount > 0)
			{
				dropCount -= Time.deltaTime;
			}
			else
			{
				dropSawSpawnPoint.position = new Vector3(Random.Range(leftPoint.position.x, rightPoint.position.x)
					, dropSawSpawnPoint.position.y, dropSawSpawnPoint.position.z);
				Instantiate(dropSaw, dropSawSpawnPoint.position, dropSawSpawnPoint.rotation);
				dropCount = timeBetweenDrops;
			}

			if (bossRight)
			{
				if (platformCount > 0)
				{
					platformCount -= Time.deltaTime;
				}
				else
				{
					rightPlatform.SetActive(true);
				}
			}
			else
			{
				if (platformCount > 0)
				{
					platformCount -= Time.deltaTime;
				}
				else
				{
					leftPlatform.SetActive(true);
				}

			}

			if (takeDamage)
			{
				currentHealth -= 1;
				if (currentHealth <= 0)
				{
					
					levelExit.SetActive(true);
					theCameraController.followTarget = true;
					gameObject.SetActive(false);
				}

				if (bossRight)
				{
					theBoss.transform.position = leftPoint.position;
				}
				else
				{
					theBoss.transform.position = rightPoint.position;
				}
				bossRight = !bossRight;
				rightPlatform.SetActive(false);
				leftPlatform.SetActive(false);
				platformCount = waitForPlatforms;

				timeBetweenDrops = timeBetweenDrops / 2f;

				takeDamage = false;
			}

		}
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			bossActive = true;
		}
	}
}

