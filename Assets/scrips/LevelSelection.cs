﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelection : MonoBehaviour
{
	public string levelToLoad;

	public bool unlock;

	public Sprite doorBottomOpen;
	public Sprite doorTopOpen;
	public Sprite doorBottomClose;
	public Sprite doorTopClose;

	public SpriteRenderer doorTop;
	public SpriteRenderer doorBottom;

	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt("levelone",1);

		if (PlayerPrefs.GetInt(levelToLoad)==1)
		{
			unlock = true;
		}
		else
		{
			unlock = false;
		}

		if (unlock)
		{
			doorTop.sprite = doorTopOpen;
			doorBottom.sprite = doorBottomOpen;
		}
		else
		{
			doorTop.sprite = doorTopClose;
			doorBottom.sprite = doorBottomClose;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			if (Input.GetButtonDown("Jump")&&unlock)
			{
				SceneManager.LoadScene(levelToLoad);
			}	
		}
		
	}
}
