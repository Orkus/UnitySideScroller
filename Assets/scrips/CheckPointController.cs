﻿using UnityEngine;

public class CheckPointController : MonoBehaviour
{

    public Sprite flagCloseSprite;
    public Sprite flagOpenSprite;

    private SpriteRenderer theSpriteRenderer;

    public bool checkPointActive;
	// Use this for initialization
	void Start ()
	{
	    theSpriteRenderer = GetComponent<SpriteRenderer>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            theSpriteRenderer.sprite = flagOpenSprite;
            checkPointActive = true;
        }
    }
}
