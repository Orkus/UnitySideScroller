using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelEnd : MonoBehaviour
{
	public Sprite flagOpen;
	private SpriteRenderer theRenderer;

	public string levelToLoad;
	public string levelToUnlock;

	public float waitToMove;
	public float waitToLoad;

	private PlayerController thePlayerController;
	private CameraController theCameraController;
	private LevelManager theLevelManager;

	private bool movePlayer;

	// Use this for initialization
	void Start ()
	{
		theRenderer = GetComponent<SpriteRenderer>();
		thePlayerController = FindObjectOfType<PlayerController>();
		theCameraController = FindObjectOfType<CameraController>();
		theLevelManager = FindObjectOfType<LevelManager>();

	}
	
	// Update is called once per frame
	void Update () {
		if (movePlayer)
		{
			thePlayerController.myRigidbody.velocity = new Vector3(thePlayerController.moveSpeed,
				thePlayerController.myRigidbody.velocity.y, 0f);
		}
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			theRenderer.sprite = flagOpen;
			//Console.WriteLine("i am here");
			//SceneManager.LoadScene(levelToLoad);
			StartCoroutine(LevelEndCo());
		}
	}

	public IEnumerator LevelEndCo()
	{
		thePlayerController.canMove = false;
		theCameraController.followTarget = false;
		theLevelManager.invincible = true;
		theLevelManager.gameMusic.Stop();
		theLevelManager.gameOverMusic.Play();

		thePlayerController.myRigidbody.velocity = Vector3.zero; 

		PlayerPrefs.SetInt("CoinCount",theLevelManager.coinCount);
		PlayerPrefs.SetInt("PlayerLives",theLevelManager.currentLives);
		PlayerPrefs.SetInt(levelToUnlock,1);
		yield return new WaitForSeconds(waitToMove);

		movePlayer = true;
		yield return new WaitForSeconds(waitToLoad);
		SceneManager.LoadScene(levelToLoad);

	}
}
