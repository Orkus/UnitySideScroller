using UnityEngine;

public class PlayerController : MonoBehaviour
{

	public float moveSpeed;
	public float platformSpeedMultiplier;
	private bool onPlatform;
	private float currentSpeed;

	public bool canMove;

	public Rigidbody2D myRigidbody;

	public float jumpSpeed;

	// to check point in space were the ground is 
	public Transform groundCheck;
	public float groundCheckRadious;
	public LayerMask whatIsGround;

	private bool isGrounded;

	private Animator myAnim;
	public Vector3 respawnPosition;
	public LevelManager theLevelManager;

	public GameObject stompBox;

	public float knockbackForce;
	public float knockbackLength;
	private float knockbackCount;

	public float invincibilityLength;
	private float invincibilityCounter;

	public AudioSource jumpSound;
	public AudioSource hurtSound;

	// Use this for initialization
	void Start()
	{
		myRigidbody = GetComponent<Rigidbody2D>();
		myAnim = GetComponent<Animator>();

		respawnPosition = transform.position;
		theLevelManager = FindObjectOfType<LevelManager>();

		currentSpeed = moveSpeed;
		canMove = true;
	}

	// Update is called once per frame
	void Update()
	{
		isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadious, whatIsGround);

		if (knockbackCount <= 0 && canMove)
		{
			if (onPlatform)
			{
				currentSpeed =moveSpeed*platformSpeedMultiplier;
			}
			else
			{
				currentSpeed = moveSpeed;
			}

			if (Input.GetAxisRaw("Horizontal") > 0f)
			{
				myRigidbody.velocity = new Vector3(currentSpeed, myRigidbody.velocity.y, 0f);
				transform.localScale = new Vector3(1f, 1f, 1f);
			}
			else if (Input.GetAxisRaw("Horizontal") < 0f)
			{
				myRigidbody.velocity = new Vector3(-currentSpeed, myRigidbody.velocity.y, 0f);
				transform.localScale = new Vector3(-1f, 1f, 1f);
			}
			else
			{
				myRigidbody.velocity = new Vector3(0f, myRigidbody.velocity.y, 0f);

			}

			if (Input.GetButtonDown("Jump") && isGrounded)
			{
				myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, jumpSpeed, 0f);
				jumpSound.Play();
			}

//			theLevelManager.invincible = false;
		}

		if (knockbackCount > 0)
		{
			knockbackCount -= Time.deltaTime;
			if (transform.localScale.x > 0)
			{
				myRigidbody.velocity = new Vector3(-knockbackForce, knockbackForce, 0f);
			}
			else 
			{
				myRigidbody.velocity = new Vector3(knockbackForce, knockbackForce, 0f);
			}
		}
		if (invincibilityCounter>0)
		{
			invincibilityCounter -= Time.deltaTime;
		}
		if (invincibilityCounter <= 0)
		{
			theLevelManager.invincible = false;
		}

		myAnim.SetFloat("speed", Mathf.Abs(myRigidbody.velocity.x));
		myAnim.SetBool("ground", isGrounded);

		if (myRigidbody.velocity.y < 0)
		{
			stompBox.SetActive(true);
		}
		else
		{
			stompBox.SetActive(false);
		}

	}

	public void Knockback()
	{
		knockbackCount = knockbackLength;
		invincibilityCounter = invincibilityLength;
		theLevelManager.invincible = true;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "KillPlane")
		{
			//gameObject.SetActive(false);
			// transform.position = respawnPosition;

			theLevelManager.Respawn();
		}

		if (other.tag == "Checkpoint")
		{
			respawnPosition = other.transform.position;
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "MovingPlatform")
		{
			transform.parent = other.transform;
			onPlatform = true;
		}
	}

	void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.tag == "MovingPlatform")
		{
			transform.parent = null;
			onPlatform = false;
		}
	}
}

